from sys import argv
import re

class Graph:

    def __init__(self, vertices):
        self.vertices = vertices
        self.graph = []

    def add_edge(self, u, v, weight):
        self.graph.append([u, v, weight])

    def find(self, parent, i):
        if parent[i] == i:
            return i
        return self.find(parent, parent[i])

    # A function that does union of two sets of x and y
    # (uses union by rank)
    def union(self, parent, rank, x, y):
        xroot = self.find(parent, x)
        yroot = self.find(parent, y)

        # Attach smaller rank tree under root of high rank tree
        # (Union by Rank)
        if rank[xroot] < rank[yroot]:
            parent[xroot] = yroot
        elif rank[xroot] > rank[yroot]:
            parent[yroot] = xroot
        # If ranks are same, then make one as root and increment
        # its rank by one
        else:
            parent[yroot] = xroot
            rank[xroot] += 1

    def PrimMST(self):
        pass





    def KruskalMST(self):
        result = []
        # Step 1:  Sort all the edges in non-decreasing order of their
        # weight.  If we are not allowed to change the given graph, we
        # can create a copy of graph
        self.graph = sorted(self.graph, key=lambda item: item[2])
        print(self.graph)

        parent = [i for i in range(self.vertices)];
        rank = [0 for _ in range(self.vertices)]

        print(parent, rank)
        i = 0  # An index variable, used for sorted edges
        j = 0  # An index variable, used for result[]
        edges = self.vertices - 1
        # Number of edges to be taken is equal to vertices-1
        while j < edges:

            # Step 2: Pick the smallest edge and increment the index
            # for next iteration
            u, v, w = self.graph[i]
            i = i + 1
            x = self.find(parent, u)
            y = self.find(parent, v)
            print(f"x,y: {x,y}")
            # If including this edge does't cause cycle, include it
            # in result and increment the index of result for next edge
            if x != y:
                j = j + 1
                result.append([u, v, w])
                self.union(parent, rank, x, y)
            # Else discard the edge

        # print the contents of result[] to display the built MST
        print("Following are the edges in the constructed MST")
        total_weight = 0
        mst = []
        for u, v, weight in result:
            # print str(u) + " -- " + str(v) + " == " + str(weight)
            mst.append([u, v, weight])
            print("%d -- %d == %d" % (u, v, weight))
            total_weight += weight
        print("Total Weight: %d" % (total_weight))
        return mst

if __name__ == "__main__":
    vertices = set([])

    gr = Graph(11)

    file = open('test.txt', 'r')
    for line in file.readlines():
        name1, name2, value = line.strip().split(' ')
        gr.add_edge(int(name1), int(name2), int(value))
        vertices.update([name1])

    vertices = sorted(list(vertices))
    print("Following are the vertices (nodes)")
    print(vertices)
    gr.KruskalMST()
    gr.PrimMST()
