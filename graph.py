import pprint
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

class Weighted_Graph(object):

    def __init__(self, edge_list_file):
        """ Set the edge list directory address """
        self.edge_list_file = edge_list_file

    def edge_dict(self):
        """ Reads in the edge list from the provided directory address and 
            creates a edge dictionary where the keys are the edges and values
            are the corresponding edge weights. In particular, to access the
            value of edge (a,b), simply type edge_dict[(a,b)]"""
        edge_dict = dict()  # dict()=empty dictionary
        edge_list = np.loadtxt(self.edge_list_file, int)  # numpy 2-d array
        for row in edge_list:
            edge_dict[(row[0], row[1])] = row[2]  # Assign keys and values
        return edge_dict

    def edge_set(self):
        """ Returns the set of edges """
        return set(self.edge_dict().keys())

    def vertex_set(self):
        """ Returns the set of vertices """
        vertex_set = set()  # set()= the empty set
        for e in self.edge_set():
            for v in e:
                vertex_set.add(v)
        return vertex_set

    def draw_graph(self):

        G = nx.read_edgelist(self.edge_list_file, nodetype=int, data=(('weight', float),))
        e = [(u, v) for (u, v, d) in G.edges(data=True)]
        pos = nx.spring_layout(G)  # positions for all nodes
        nx.draw_networkx_nodes(G, pos, node_size=250)  # nodes
        nx.draw_networkx_edges(G, pos, edgelist=e, width=1)  # edges

        labels = nx.get_edge_attributes(G, 'weight')
        nx.draw_networkx_labels(G, pos, font_size=10, font_family='sans-serif')
        nx.draw_networkx_edge_labels(G, pos, edge_labels=labels)
        plt.axis('off')
        plt.show()


if __name__ == "__main__":
    obj2 = Weighted_Graph("test.txt")
    obj = Weighted_Graph("output.txt")
    obj2.draw_graph()
    obj.draw_graph()
    pp = pprint.PrettyPrinter()
    pp.pprint(obj.vertex_set())
