# Kruskal's Alghoritm


### Finds a minimum spanning forest of an undirected edge-weighted graph. 
### If the graph is connected, it finds a minimum spanning tree.

# How to run?

- Create `test.txt` file with test values
- Run `python main.py` to generate vertices
- Run `python graph.py` to generate graph


# Requirements

- `Python3`
- `Numpy, Matplotlib and networkx for graph`

*Scripted for Uni classes - no updates. 